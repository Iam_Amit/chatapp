const path = require("path");
const http = require("http");
const express = require("express");
const socketio = require("socket.io");
const Filter = require('bad-words')

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const port = 3000;
const publicDirectoryPath = path.join(__dirname, "../public");

app.use(express.static(publicDirectoryPath));

io.on("connection", (socket) => {
    console.log('Socket Id => ', socket.id);
    // console.log("New websocket connection "); 

    socket.emit("message", "WelcoMe");
    socket.broadcast.emit('message',' New user has joined the grp !')// to emit the message to everyone except that particular one
    
    socket.on("sendMessage", (message, callback) => {
        const filter = new Filter({ placeHolder: 'x'})

        if(filter.isProfane(message)) {
            
            console.log(filter.clean(message))
            return callback(filter.clean(message))
            // return callback('Profnity is not Allowed !')
        }

    io.emit("message", message)
    callback()
  });

socket.on('sendLocation', (coords, callback) => {

    // io.emit('message', `Location: ${coords.latitude},${coords.longitude}`)
    io.emit('message', `https://google.com/maps?q=${coords.latitude},${coords.longitude}`)
    callback()
});

  socket.on('disconnect', () => {
      console.log('User Disconnected !')
      io.emit('message', 'User left the chat !');
  })
});

server.listen(port, () => {
  console.log(" The running port is  => ", port);
});
//When we require the library we get the funx back
